export const daysOrder = ['mo','tu','we','th','fr','sa','su'];
export const minToHours = mins => Math.floor(mins / 60);

export const generateEmptyDay = () => {
    const dayInArray = [];
    for (let i = 0; i < 24; i++) {
        dayInArray.push(null);
    }
    return dayInArray
};

 export function convertTo24Cells(daySchedule) {
    const dayInArray = generateEmptyDay();

    daySchedule.forEach(timeline => {
        const begin = minToHours(timeline.bt);
        const end = minToHours(timeline.et);

        for (let i = begin; i <= end; i++) {
            let cell = {};
            i === begin ? cell['bt'] = timeline.bt : cell['bt'] = i * 60;
            i === end ? cell['et'] = timeline.et : cell['et'] = (i * 60) + 59;

            dayInArray[i] = cell;
        }
    });

    return dayInArray;
}
