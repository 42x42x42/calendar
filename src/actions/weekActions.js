export const SET_SINGLE_CELL_TIMEOUT = 'SET_SINGLE_CELL_TIMEOUT';
export const SET_FULL_DAY_TIMEOUT = 'SET_FULL_DAY_TIMEOUT';
export const CLEAR_FULL_DAY_TIMEOUT = 'CLEAR_FULL_DAY_TIMEOUT';
export const CLEAR_ALL_CALENDAR = 'CLEAR_ALL_CALENDAR';
export const CLEAR_SINGLE_CELL_TIMEOUT = 'CLEAR_SINGLE_CELL_TIMEOUT';


export function setSingleCellTimeout(cell) {
    return {
        type: SET_SINGLE_CELL_TIMEOUT,
        cell
    }
}

export function clearSingleCell(cell) {
    return {
        type: CLEAR_SINGLE_CELL_TIMEOUT,
        cell
    }
}

export function setFullDayTimeout(day) {
    return {
        type: SET_FULL_DAY_TIMEOUT,
        day
    }
}

export function clearFullDayTimeout(day) {
    return {
        type: CLEAR_FULL_DAY_TIMEOUT,
        day
    }
}

export function clearAllCalendar() {
    return {type: CLEAR_ALL_CALENDAR}
}