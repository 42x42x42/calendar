export const ACTIVATE_HOVER = 'ACTIVATE_HOVER';
export const DE_ACTIVATE_HOVER = 'DE_ACTIVATE_HOVER';

export function activateHover() {
    return {
        type: ACTIVATE_HOVER,
    }
}

export function deActivateHover() {
    return {
        type: DE_ACTIVATE_HOVER,
    }
}
