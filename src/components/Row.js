import React, {PureComponent} from "react";
import {connect} from 'react-redux';
import Cell from './Cell';
import {setFullDayTimeout, clearFullDayTimeout} from '../actions/weekActions';

class Row extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            isDayEmpty: this.checkIsDayEmpty(),
            isDayFull: this.checkIsDayFull()
        };
    }

    checkIsDayEmpty = () => (this.props.week[this.props.day].every(cell => cell === null));
    checkIsDayFull = () => (this.props.week[this.props.day].every(cell => cell !== null));

    componentDidUpdate() {
        const isDayEmpty = this.checkIsDayEmpty();
        const isDayFull = this.checkIsDayFull();
        this.setState({isDayEmpty, isDayFull});
    };

    render() {
        const {day, week, setFullDayTimeout, clearFullDayTimeout} = this.props;

        return (
            <div className='row'>
                <div className={`cell double-cell ${this.state.isDayEmpty ? '' : 'selected'}`}>
                    {day.toLocaleUpperCase()}
                </div>
                <div className={`cell double-cell all-cell ${this.state.isDayFull ? 'fullDay' : ''}`}
                     onClick={this.state.isDayEmpty ? () => setFullDayTimeout(day) : () => clearFullDayTimeout(day)}
                >
                    {this.state.isDayFull && 'All'}
                </div>
                {week[day].map((timing, hour) => {
                    return (
                        <Cell
                            day={day}
                            timing={timing}
                            hour={hour}
                            key={day + hour}
                        />
                    )
                })}

            </div>
        )
    }
}

const mapStateToProps = (state) => ({week: state.week});

export default connect(mapStateToProps, {setFullDayTimeout, clearFullDayTimeout})(Row);