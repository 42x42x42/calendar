import React, {PureComponent} from "react";
import {connect} from 'react-redux';
import {setSingleCellTimeout, clearSingleCell} from '../actions/weekActions'
import {activateHover, deActivateHover} from '../actions/controlsActions';

class Cell extends PureComponent {
    render() {
        const {day, hour, timing, setSingleCellTimeout, clearSingleCell, controls, activateHover, deActivateHover} = this.props;
        const bt = timing ? timing.bt : hour * 60;
        const et = timing ? timing.et : hour * 60 + 59;
        return (
            <div
                className={`cell ${timing ? 'selected' : 'unselected'}`}
                onMouseEnter={(e) => {
                    if (e.buttons === 1 && controls.hover) {
                        setSingleCellTimeout({day, hour, bt, et})
                    }
                }}

                onMouseDown={
                    timing ?
                        () => clearSingleCell({day, hour})
                        :
                        () => {
                            activateHover();
                            setSingleCellTimeout({day, hour, bt, et})
                        }
                }
                onMouseUp={deActivateHover}
            >
                {' '}
            </div>
        )
    }
}

const mapStateToProps = state => ({week: state.week, controls: state.controls});

export default connect(mapStateToProps, {setSingleCellTimeout, clearSingleCell, activateHover, deActivateHover})(Cell);