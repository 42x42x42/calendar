import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import Row from './Row';
import {clearAllCalendar} from '../actions/weekActions'
import {daysOrder} from '../data/dataHelpers';
import '../styles/App.css';


class App extends PureComponent {
    constructor(props) {
        super(props);
        this.hours = [];
        for (let i = 0; i < 24; i++) {
            this.hours.push(i);
        }
    }

    render() {
        return (
            <div className="App">
                <h4>SET SCHEDULE</h4>
                <div className='row header'>
                    <div className="cell double-cell">{' '}</div>
                    <div className="cell double-cell">All day</div>
                    {
                        this.hours.map((index) =>
                            <div className="cell header-cell"
                                 key={index}>
                                {index <= 9 ? '0' + index : index}:00
                            </div>
                        )
                    }
                </div>
                <Row day={daysOrder[0]}/>
                <Row day={daysOrder[1]}/>
                <Row day={daysOrder[2]}/>
                <Row day={daysOrder[3]}/>
                <Row day={daysOrder[4]}/>
                <Row day={daysOrder[5]}/>
                <Row day={daysOrder[6]}/>
                <div className='button-group'>
                    <button className='btn' onClick={this.props.clearAllCalendar}>Clear All</button>
                    <button>Save changes</button>
                </div>
                <p className='reminder'>* do not forget to check 'readme' file with comments to this task</p>
            </div>
        );
    }
}

const mapStateToProps = (state) => state;

export default connect(mapStateToProps, {clearAllCalendar})(App);
