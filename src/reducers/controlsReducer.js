import {ACTIVATE_HOVER,DE_ACTIVATE_HOVER} from '../actions/controlsActions';

const defState = {hover: false};

export default function controlsReducer(state = defState, action) {

    switch(action.type){

        case ACTIVATE_HOVER:
            return {hover: true};
        case DE_ACTIVATE_HOVER:
            return {hover: false};
        default:
            return state
    }

}