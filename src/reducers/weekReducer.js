import {
    SET_SINGLE_CELL_TIMEOUT,
    SET_FULL_DAY_TIMEOUT,
    CLEAR_FULL_DAY_TIMEOUT,
    CLEAR_ALL_CALENDAR,
    CLEAR_SINGLE_CELL_TIMEOUT
} from '../actions/weekActions';
import starterData from "../data/data";
import {daysOrder, convertTo24Cells, generateEmptyDay} from '../data/dataHelpers'

const defState = () => {
    const loadedState = JSON.parse(JSON.stringify(starterData));
    const state = {};

    for (let key in loadedState) {
        state[key] = convertTo24Cells(loadedState[key]);
    }
    return state;
};


export default function weekReducer(state = defState(), action) {
    switch (action.type) {

        case SET_SINGLE_CELL_TIMEOUT:
            const {day, hour, bt, et} = action.cell;
            const cell = {bt, et};
            const newState = {...state}; //intentional shallow copy of state
            newState[day][hour] = cell;
            return newState;

        case CLEAR_SINGLE_CELL_TIMEOUT:
            const newState2 = {...state};  //intentional shallow copy of state
            newState2[action.cell.day][action.cell.hour] = null;
            return newState2;

        case SET_FULL_DAY_TIMEOUT:
            const fullDay = {};
            const fullDayTimeLine = [{
                bt: 0,
                et: 1439
            }];
            fullDay[action.day] = convertTo24Cells(fullDayTimeLine);
            return {...state, ...fullDay}; //intentional shallow copy of state

        case CLEAR_FULL_DAY_TIMEOUT:
            const clearedDay = {};
            clearedDay[action.day] = generateEmptyDay();
            return {...state, ...clearedDay}; //intentional shallow copy of state

        case CLEAR_ALL_CALENDAR:
            const clearState = {};
            daysOrder.forEach(el => clearState[el] = generateEmptyDay());
            return clearState;

        default:
            return state;
    }
}
