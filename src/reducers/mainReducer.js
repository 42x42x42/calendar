import {combineReducers} from 'redux';
import controlsReducer from './controlsReducer';
import weekReducer from './weekReducer';

export default combineReducers({
    week: weekReducer,
    controls: controlsReducer
})